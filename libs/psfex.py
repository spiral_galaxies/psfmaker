#! /usr/bin/env python

import subprocess
from os import path
from astropy.io import fits


def call_psfex(cat_name, psf_image_size, sampling):
    call_string = "psfex %s" % cat_name
    call_string += " -c %s" % path.join(path.dirname(__file__), "default.psfex")
    call_string += " -PSF_SIZE %i" % psf_image_size
    call_string += " -PSF_SAMPLING %r" % sampling
    subprocess.call(call_string, shell=True)


def extract_psf(cat_name, psf_name):
    cat_name_no_ext = path.splitext(path.basename(cat_name))[0]
    path_to_psf = path.join(path.dirname(cat_name), cat_name_no_ext+".psf")
    hdu = fits.open(path_to_psf)
    psf_data = hdu[1].data[0][0][0]
    sampling = hdu[1].header["PSF_SAMP"]
    psf_fwhm = hdu[1].header["PSF_FWHM"]
    psf_header = fits.Header([("SAMPLING", sampling), ("PSF_FWHM", psf_fwhm)])
    fits.PrimaryHDU(data=psf_data, header=psf_header).writeto(psf_name, overwrite=True)


def compute_psf(cat_name, psf_name, x_cen, y_cen):
    """
    Computes the psf for a given position on the image using the
    2nd degree polynomial expansion
    """
    cat_name_no_ext = path.splitext(path.basename(cat_name))[0]
    path_to_psf = path.join(path.dirname(cat_name), cat_name_no_ext+".psf")
    hdu = fits.open(path_to_psf)

    x_zero = hdu[1].header["POLZERO1"]   #
    x_scale = hdu[1].header["POLSCAL1"]  # Scales and zero points
    y_zero = hdu[1].header["POLZERO2"]   #
    y_scale = hdu[1].header["POLSCAL2"]  #
    x = (x_cen - x_zero) / x_scale  # Scaled and zero-shifted values of parameters
    y = (y_cen - y_zero) / y_scale  #

    poly = hdu[1].data[0][0]
    total_psf = poly[0]  # Constant value
    if len(poly) > 1:
        c_x = hdu[1].data[0][0][1]  # linear component along x-axis
        c_y = hdu[1].data[0][0][2]  # linear component along y-axis
        total_psf += c_x*x + c_y*y
    if len(poly) > 3:
        c_x2 = hdu[1].data[0][0][3]  # second degree component along x-axis
        c_xy = hdu[1].data[0][0][4]  # x times y
        c_y2 = hdu[1].data[0][0][5]  # second degreen along y-axis
        total_psf += c_x2*x**2 + c_xy*x*y + c_y2*y**2
    sampling = hdu[1].header["PSF_SAMP"]
    psf_fwhm = hdu[1].header["PSF_FWHM"]
    psf_header = fits.Header([("SAMPLING", sampling), ("PSF_FWHM", psf_fwhm),
                              ("XCEN", round(x_cen, 2)), ("YCEN", round(y_cen, 2))])
    fits.PrimaryHDU(data=total_psf, header=psf_header).writeto(psf_name, overwrite=True)
