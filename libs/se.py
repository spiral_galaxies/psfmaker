#! /usr/bin/env python

from os import path
import os
import subprocess
import shutil
import numpy as np
from astropy.io import fits


work_dir = path.join(path.dirname(__file__), "../work_dir")


class SExCatalogue(object):
    def __init__(self, catFileName):
        self.objectList = []
        self.legend = []
        self.index = 0  # For iterations
        # Parse SE catalogue with arbitrary number of objects and parameters
        for line in open(catFileName):
            sLine = line.strip()
            if sLine.startswith("#"):
                # legend line
                self.legend.append(sLine.split()[2])
                continue
            params = [float(p) for p in line.split()]
            obj = {key: value for key, value in zip(self.legend, params)}
            self.objectList.append(obj)
        self.numOfObjects = len(self.objectList)

    def get_median_value(self, parameter):
        return np.median([obj[parameter] for obj in self.objectList])

    def get_all_values(self, parameter):
        return np.array([obj[parameter] for obj in self.objectList])

    def __iter__(self):
        return self

    def __next__(self):
        """ Iteration method """
        if self.index < self.numOfObjects:
            self.index += 1
            return self.objectList[self.index-1]
        else:
            self.index = 1
            raise StopIteration


def call_sextractor(fits_name, config_name=None, params=None):
    """
    Function calls a SExtractor.
    Arguments:
        fits_name -- name of a fits file to analyse
        params -- additional params to override the values from the config file
    """
    config_name = path.join(path.dirname(__file__), "default.sex")
    params_file_name = path.join(path.dirname(__file__), "default.param")
    conv_file_name = path.join(path.dirname(__file__), "default.conv")
    # Check if files exist
    if not path.exists(fits_name):
        raise IOError("%s file not found" % fits_name)
    if not path.exists(config_name):
        raise IOError("%s file not found" % config_name)

    # Create a command line for SExtractor
    call_string = "sex %s" % fits_name
    call_string += " -c %s" % config_name
    call_string += " -PARAMETERS_NAME %s" % params_file_name
    call_string += " -FILTER_NAME %s" % conv_file_name
    if params is not None:
        for key, value in params.items():
            call_string += " -%s %s" % (key, value)

    # Call SExtractor
    rc = subprocess.call(call_string, shell=True)
    return rc


def find_fwhm(fits_name):
    """
    Function finds a median value of FWHM of a given image using SExtractor
    """
    catalogue_name = path.join(work_dir, "tmp.cat")
    params = {"CATALOG_NAME": catalogue_name, "CATALOG_TYPE": "ASCII_HEAD"}
    call_sextractor(fits_name, params=params)
    cat = SExCatalogue(catalogue_name)
    if cat.numOfObjects > 9:
        fwhm = cat.get_median_value("FWHM_IMAGE")
    else:
        print("Not enough objects to build a proper PSF")
        fwhm = None
    os.remove(catalogue_name)
    return fwhm


def prepare_catalogue_for_psfex(args, psf_image_size):
    # Read image scale to compute aperture size.
    header = fits.getheader(args.image)
    kwords = ["CD1_1", "CD1_2", "CD2_1", "CD2_2"]
    scales = []
    for key in kwords:
        if key in header:
            scales.append(abs(float(header[key])))
    if len(scales) != 0:
        image_scale = 3600 * np.max(scales)  # arcsec per pixel
        print("Image scale: %1.3f ''/pix" % image_scale)
    else:
        print("Scale parameter was not found in the header. Using scale=1 ''/pix")
        image_scale = 1.0
    # Recommended aperture size is 5''
    aperture_size_pixels = 5 / image_scale

    # Read gain value from fits file or use command line option
    if args.gain < 0:
        gain = float(header["GAIN"])
        print("Gain value: %1.2f" % gain)
    else:
        gain = args.gain

    # Read saturation level
    if args.saturate < 0:
        saturation = 0.95 * float(header["SATURATE"])
        print("Saturation level set to %1.2f" % saturation)
    else:
        saturation = 0.95 * args.saturate

    # Now lets make a SExtractor binary catalogue
    # Modify parameters list
    par_file_name = path.join(work_dir, "for_psfex.param")
    default_params_file_name = path.join(path.dirname(__file__), "default.param")
    shutil.copy(default_params_file_name, par_file_name)
    with open(par_file_name, "a") as par_file:
        par_file.write("VIGNET(%i,%i)\n" % (psf_image_size, psf_image_size))

    catalogue_name = path.join(work_dir, "catalogue.cat")
    params = {"PARAMETERS_NAME": par_file_name, "PHOT_APERTURES": aperture_size_pixels,
              "GAIN": gain, "SATUR_LEVEL": saturation, "CATALOG_NAME": catalogue_name}
    call_sextractor(args.image, params=params)
    return catalogue_name
