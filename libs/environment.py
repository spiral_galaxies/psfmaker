#! /usr/bin/env python

import os
from os import path
import shutil


work_dir = path.join(path.dirname(__file__), "../work_dir")


def setup(args):
    """
    Prepare working directory
    """
    if path.exists(work_dir):
        shutil.rmtree(work_dir)
    os.makedirs(work_dir)

    if args.generate > 0:
        out_path = path.join(args.outdir, "variations")
        if path.exists(out_path):
            shutil.rmtree(out_path)
        os.makedirs(out_path)


def cleanup():
    """
    Clean working directory when finished
    """
    if path.exists(work_dir):
        shutil.rmtree(work_dir)
