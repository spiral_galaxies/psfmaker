#!/usr/bin/env python

import argparse
from os import path
from random import uniform
from astropy.io import fits
from libs.environment import setup
from libs.environment import cleanup
from libs.se import find_fwhm
from libs.se import prepare_catalogue_for_psfex
from libs.psfex import call_psfex
from libs.psfex import extract_psf
from libs.psfex import compute_psf


def main(args):
    setup(args)
    # At first we want to compute a PSF image size. Let it be equal to 10*FWHM / sampling
    fwhm = find_fwhm(args.image)
    if fwhm is None:
        cleanup()
        return
    if fwhm > args.maxfwhm:
        cleanup()
        return
    print("Image FWHM: %1.2f" % fwhm)
    psf_image_size = int(args.size * fwhm / args.sampling)
    # PSF image should be odd
    if psf_image_size % 2 == 0:
        psf_image_size += 1
    print("PSF image size: %i" % psf_image_size)

    # Create a binary SExtractor catalogue for psfex use
    cat_name = prepare_catalogue_for_psfex(args, psf_image_size)
    call_psfex(cat_name, psf_image_size, args.sampling)
    if args.psf_name is None:
        psf_name = path.splitext(args.image)[0] + "_psf.fits"
    else:
        psf_name = args.psf_name
    extract_psf(cat_name, psf_name)

    # Generate some psfs with random locations. These psfs will have somewhat
    # different shapes due to position-dependance of their parameters
    if args.bound is None:
        xmin = 0
        ymin = 0
        ymax, xmax = fits.getdata(args.image).shape
    else:
        xmin, xmax, ymin, ymax = args.bound
    for idx in range(args.generate):
        x_cen = uniform(xmin, xmax)
        y_cen = uniform(ymin, ymax)
        out_name = path.join(args.outdir, "variations", "psf_%i.fits" % idx)
        compute_psf(cat_name, out_name, x_cen, y_cen)
    cleanup()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("image", type=str, help="Image name")
    parser.add_argument("--sampling", type=float, default=1.0,
                        help="Sampling step of PSF [pix]. Default 1.0")
    parser.add_argument("--gain", type=float, default=-1.0,
                        help="Gain value. Necessary if there is no GAIN key in the fits header.")
    parser.add_argument("--saturate", type=float, default=-1.0,
                        help="Saturation level. Necessary if there is no SATURATE key in the fits header.")
    parser.add_argument("--psf-name", default=None,
                        help="Name of PSF file. Default *_psf.fits")
    parser.add_argument("--generate", type=int, default=0,
                        help="Generate N PSF in a random locations within a bounding box")
    parser.add_argument("--bound", nargs=4, type=int,
                        help="Bounding box for random PSF locations xmin xmax ymin ymax")
    parser.add_argument("--outdir", default='results',
                        help="Output directory")
    parser.add_argument("--size", type=float, default=15,
                        help="Resilting PSF image size (if units PSF FWHM). Default is 15.")
    parser.add_argument("--maxfwhm", type=float, default=10)
    args = parser.parse_args()
    main(args)
