# README #

A tool for creating PSF images based on SExtractor and PSEx packages.

# Requirements #

    - sextractor
	- psfex
	- python 3.*
	- astropy
	- numpy

# Install #
Just clone this repository. Everything should work just fine as soon as all
requirements are satisfied.

# Usage #

**positional arguments:**

    image                 Image name

**optional arguments:**

    --sampling SAMPLING           Sampling step of PSF [pix]. Default 1.0
    --gain GAIN                   Gain value. Necessary if there is no GAIN key in the
                                  fits header.
    --saturate SATURATE           Saturation level. Necessary if there is no SATURATE
	                              key in the fits header.
    --psf-name PSF_NAME           Name of PSF file. Default *_psf.fits
    --generate GENERATE           Generate N PSF in a random locations within a bounding box
    --bound xmin xmax ymin ymax   Bounding box for random PSF locations xmin xmax ymin (by default the entire image is taken)


## Examples ##
Simpliest case:

    ./psfmaker.py field.fits

Create an overampled PSF

	./psfmaker.py field.fits --sampling 0.5

Create a 10 PSFs with a random locations inside an image box with xmin=100 xmax=200 ymin=300 ymax=400

	./psfmaker.py field.fits --generate 10 --bounx 100 200 300 400

# Output
As a result of the script run, a FITS-file named `*_psf.fits` with PSF model will appear in the
script direcory.

PSFs with random locations are stored in **variations** directory
